import React, { Component } from 'react';
import './App.css';

import Calendar from './Components/Calendar/';


const style = {
  position: "relative",
  
}

class App extends Component {
 /* onDayClick = (e, day) => {
    alert(day);
  }*/
  
  render() {
    return (
      <div className="App">
         <div className='calendar-layout'>
           <div className='calendar-layout-content'>
             <div className='calendar-app'  >
                  <Calendar style={style}/>
                  { /* onDayClick={(e, day)=> this.onDayClick(e, day)} */}    
           </div>
           <div className='calender-style-date'  >
                   <p style={{fontSize:"9px"}}>10:30 AM</p>
                   <p style={{fontSize:"13px"}}>Java Assignment last date</p>
                   <p style={{fontSize:"9px"}}>10:30 PM</p>
                   <p style={{fontSize:"13px"}}>New Activity Started</p>
           </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
